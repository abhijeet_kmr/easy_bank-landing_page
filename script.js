let btn = document.querySelector(".toggle");
let icon = btn.querySelector(".fa-bars");
btn.onclick = function () {
  if (icon.classList.contains("fa-bars")) {
    icon.classList.replace("fa-bars", "fa-times");
  } else {
    icon.classList.replace("fa-times", "fa-bars");
  }
};
const navMobile = document.querySelector(".navlist-mobile");
document.querySelector(".toggle").addEventListener("click", () => {
  let display = navMobile.style.display;
  console.log(display, "clicked");
  if (display === "block") {
    navMobile.style.display = "";
  } else {
    navMobile.style.display = "block";
  }
});
